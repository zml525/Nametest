package com.mj.nametest.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mj.nametest.util.HttpUtils;

/**
 * 姓名测试打分
 * @author zhaominglei
 * @date 2015-4-4
 * 
 */
public class NametestService extends BaseService {
	@SuppressWarnings("unused")
	private static final String TAG = NametestService.class.getSimpleName();
	private static final String SHEUP_DAFEN_URL = "http://www.sheup.com/xingming_dafen.php"; //三藏算命网
	private static final String M_SHEUP_DAFEN_URL = "http://m.sheup.com/xingming_ceshi_1.php"; //三藏算命手机版
	private static final String NOTE = "姓名测试打分依据姓名的音、形、义，按易学的象、数、理为依据，综合阴阳五行，预测分析名字中对特定人和事物吉凶与变化趋势，测算结果仅供娱乐，切勿迷信！";
	public String getNametestInfo(String familyname, String firstname) {
		if (familyname == null || familyname.equals("") 
				|| firstname == null || firstname.equals("")) {
			return null;
		}
//		String html = dafen("万", "里");
//		Document document = Jsoup.parse(html);
//		//分数
//		Elements div_pingfen_right_ass = document.getElementsByClass("pingfen_right_ass");
//		if (div_pingfen_right_ass != null && !div_pingfen_right_ass.isEmpty()) {
//			Element element = div_pingfen_right_ass.first();
//			System.out.println(element.text());
//		}
//		//三才配置
//		Elements div_content_text = document.getElementsByClass("content_text");
//		if (div_content_text != null && !div_content_text.isEmpty()) {
//			int i=0;
//			for (Element element : div_content_text) {
//				String content = element.html();
//				if (i > 0 && i < 9) {
//					if (i == 1) {
//						System.out.println(content.replaceAll("小贴士：更多更精准的免费测试算命项目欢迎访问【139算命网】", ""));
//					}
//					System.out.println(content);
//				}
//				i++;
//			}
//		}
		String html = mdafen(familyname, firstname);
		if (html == null || html.equals("")) {
			return null;
		}
		Document document = Jsoup.parse(html);
		Elements elements = document.getElementsByClass("sanzang_subs");
		if (elements != null && !elements.isEmpty()) {
			StringBuilder result = new StringBuilder();
			int i = 0;
			for (Element element : elements) {
				String content = element.html();
				if (i < 8) {
					result.append(content);
				}
				i++;
			}
			result.append(NOTE);
			return result.toString();
		}
		return null;
	}
	
	public static String dafen(String xing, String ming){
		StringBuilder param = new StringBuilder();
		param.append("xingx=").append(HttpUtils.encodeURI(xing, "gb2312"));
		param.append("&mingx=").append(HttpUtils.encodeURI(ming, "gb2312"));
		param.append("&Submit=%D0%D5%C3%FB%B2%E2%CA%D4%B4%F2%B7%D6");
		String html = HttpUtils.doPostForSheup(SHEUP_DAFEN_URL, param.toString(), "gbk");
		return html;
	}

	public static String mdafen(String xing, String ming){
		StringBuilder param = new StringBuilder();
		param.append("ceshi_xing=").append(HttpUtils.encodeURI(xing, "gb2312"));
		param.append("&ceshi_ming=").append(HttpUtils.encodeURI(ming, "gb2312"));
		param.append("&dafen=%D0%D5%C3%FB%B2%E2%CA%D4%B4%F2%B7%D6");
		String html = HttpUtils.doPostForMSheup(M_SHEUP_DAFEN_URL, param.toString(), "gbk");
		return html;
	}
}
