package com.mj.nametest.activity;

import java.util.Timer;
import java.util.TimerTask;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.nametest.R;
import com.mj.nametest.service.NametestService;
import com.mj.nametest.util.NetUtils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
/**
 * 姓名测试打分
 * @author zhaominglei
 * @date 2015-4-4
 * 
 */
public class MainActivity extends Activity implements OnClickListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = MainActivity.class.getSimpleName();
	private NametestService nametestService = new NametestService();
	private boolean isExit = false;
	private TimerTask timerTask;
	private LinearLayout miniAdLinearLayout; //迷你广告
	private EditText familynameText; //姓输入框
	private String familyname; //姓
	private EditText firstnameText; //名输入框
	private String firstname; //名
	private Button queryBtn; //测试打分
	private ProgressBar queryProgressBar; //查询进度条
	private TextView resultView; //结果
	private Button appOffersButton; //推荐应用
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		init();
	}

	private void init() {
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.miniAdLinearLayout);
		familynameText = (EditText)findViewById(R.id.nametest_familyname_edt);
		firstnameText = (EditText)findViewById(R.id.nametest_firstname_edt);
		queryBtn = (Button)findViewById(R.id.nametest_query_btn);
		queryProgressBar = (ProgressBar)findViewById(R.id.nametest_query_progressbar);
		resultView = (TextView)findViewById(R.id.nametest_result);
		appOffersButton = (Button)findViewById(R.id.appOffersButton);
		
		queryBtn.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		
		appControl = AppControl.getInstance();
		appControl.init(MainActivity.this, "5f57797869676631NYlECEHSF2kY0rLyNwY+wK8K/iACMb/kH1MMBMNDRZOoo8H0wQ", "木蚂蚁");
		appControl.loadPopAd(MainActivity.this);
		appControl.showPopAd(MainActivity.this, 60 * 1000);
		appControl.showInter(MainActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(MainActivity.this, "5f57797869676631NYlECEHSF2kY0rLyNwY+wK8K/iACMb/kH1MMBMNDRZOoo8H0wQ", "木蚂蚁");
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.nametest_query_btn:
			familyname = familynameText.getText().toString();
			if (familyname == null || familyname.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.nametest_familyname_hint, Toast.LENGTH_SHORT).show();
				return;
			}
			firstname = firstnameText.getText().toString();
			if (firstname == null || firstname.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.nametest_firstname_hint, Toast.LENGTH_SHORT).show();
				return;
			}
			resultView.setText("");
			
			getNametestInfo();
			break;

		case R.id.appOffersButton:
			access.openWALL(MainActivity.this);
			break;
			
		default:
			break;
		}
	}
	
	private void getNametestInfo() {
		if (NetUtils.isConnected(getApplicationContext())) {
			queryProgressBar.setVisibility(View.VISIBLE);
			new NametestInfoAsyncTask().execute("");
		} else {
			Toast.makeText(getApplicationContext(), R.string.net_error, Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onBackPressed() {
		if (isExit) {
			MainActivity.this.finish();
		} else {
			isExit = true;
			Toast.makeText(MainActivity.this, R.string.exit_msg, Toast.LENGTH_SHORT).show();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			};
			new Timer().schedule(timerTask, 2*1000);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	public class NametestInfoAsyncTask extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			return nametestService.getNametestInfo(familyname, firstname);
		}

		@Override
		protected void onPostExecute(String result) {
			queryProgressBar.setVisibility(View.GONE);
			if (result != null && !result.equals("")) {
				resultView.setText(Html.fromHtml(result));
			} else {
				Toast.makeText(getApplicationContext(), R.string.nametest_error1, Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}
}
